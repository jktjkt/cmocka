Name: ${PROJECT_NAME}
Description: The cmocka unit testing library
Version: ${PROJECT_VERSION}
Libs: -L${LIB_INSTALL_DIR} -lcmocka
Cflags: -I${INCLUDE_INSTALL_DIR}
